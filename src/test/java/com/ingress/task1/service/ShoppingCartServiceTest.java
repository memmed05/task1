package com.ingress.task1.service;

import com.ingress.task1.dto.AddProductToCartDto;
import com.ingress.task1.dto.ProductDto;
import com.ingress.task1.dto.ShoppingCartRequestDto;
import com.ingress.task1.dto.ShoppingCartResponseDto;
import com.ingress.task1.model.Category;
import com.ingress.task1.model.Product;
import com.ingress.task1.model.ProductDetails;
import com.ingress.task1.model.ShoppingCart;
import com.ingress.task1.repository.ProductRepository;
import com.ingress.task1.repository.ShoppingCartRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class ShoppingCartServiceTest {

    @InjectMocks
    private ShoppingCartService shoppingCartService;

    @Mock
    private ShoppingCartRepository shoppingCartRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private RedisTemplate redisTemplate;

    @Mock
    private ValueOperations valueOperations;


    @Test
    void createShoppingCart() {

        Long shoppingCartId = 1L;
        String cartName = "lorem";

        ShoppingCartRequestDto requestDto = new ShoppingCartRequestDto(cartName);

        ShoppingCart cart = ShoppingCart.builder()
                .name(cartName)
                .build();

        ShoppingCart savedCart = ShoppingCart.builder()
                .name(cartName)
                .id(shoppingCartId)
                .products(new ArrayList<>())
                .build();

        when(shoppingCartRepository.save(cart)).thenReturn(savedCart);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        Long saveId = shoppingCartService.createShoppingCart(requestDto);

        assertThat(savedCart.getId()).isEqualTo(saveId);
        verify(shoppingCartRepository, times(1)).save(cart);
        verify(redisTemplate, times(1)).opsForValue();
        verify(valueOperations, times(1)).set(any(), any());

    }


    @Test
    void givenValidProductAddProductToShoppingCartThenSuccess() {
        Long shoppingCartId = 1L;
        Long productId = 10L;
        String redisKey = "shoppingCart-" + shoppingCartId;
        AddProductToCartDto dto = new AddProductToCartDto(productId);
        List<Product> products = new ArrayList<>();
        Product product = new Product(
                productId,
                "lorem",
                100,
                "desc",
                Category.builder().name("cat").build(),
                ProductDetails.builder().color("red").imageUrl("uri").build()
        );
        ShoppingCart cart = ShoppingCart.builder().id(shoppingCartId).products(new ArrayList<>()).build();

        ShoppingCartResponseDto responseDto = new ShoppingCartResponseDto("lorem", new ArrayList<>());
        when(shoppingCartRepository.findById(shoppingCartId)).thenReturn(Optional.of(cart));
        when(productRepository.getReferenceById(productId)).thenReturn(product);
        when(shoppingCartRepository.getReferenceById(shoppingCartId)).thenReturn(cart);
        when(shoppingCartRepository.save(cart)).thenReturn(cart);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.get(redisKey)).thenReturn(responseDto);

        List<ProductDto> result = shoppingCartService.addProductToShoppingCart(shoppingCartId, dto);

        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getName()).isEqualTo(product.getName());

        verify(shoppingCartRepository, times(1)).findById(shoppingCartId);
        verify(productRepository, times(1)).getReferenceById(productId);
        verify(shoppingCartRepository, times(1)).getReferenceById(shoppingCartId);
        verify(shoppingCartRepository, times(1)).save(cart);
        verify(redisTemplate, times(2)).opsForValue();
        verify(valueOperations, times(1)).set(any(), any());
        verify(valueOperations, times(1)).get(redisKey);


    }

    @Test
    void givenInValidCartAddProductToShoppingCartThenEx() {

        when(shoppingCartRepository.findById(1L)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> shoppingCartService.addProductToShoppingCart(1L, any()))
                .isInstanceOf(RuntimeException.class);
        verify(shoppingCartRepository, times(1)).findById(1L);
    }


    @Test
    void givenValidIdDeleteProductFromShoppingCartThenSuccess() {
        Long id = 1L;
        String redisKey = "shoppingCart-" + id;
        Long productId = 10L;
        Product product = new Product(
                productId,
                "lorem",
                100,
                "desc",
                Category.builder().name("cat").build(),
                ProductDetails.builder().color("red").imageUrl("uri").build()
        );
        List<Product> products = new ArrayList<>();
        products.add(product);
        ShoppingCart cart = ShoppingCart.builder().id(id).products(products).build();
        List<ProductDto> productDtoList = List.of(ProductDto.builder()
                .name(product.getName())
                .price(product.getPrice())
                .color(product.getProductDetails().getColor())
                .description(product.getDescription())
                .imageUrl(product.getProductDetails().getImageUrl())
                .categoryName(product.getCategory().getName())
                .build());

        ShoppingCartResponseDto responseDto = new ShoppingCartResponseDto("lorem", new ArrayList<>());


        when(shoppingCartRepository.findById(id)).thenReturn(Optional.of(cart));
        when(shoppingCartRepository.getReferenceById(id)).thenReturn(cart);
        when(productRepository.getReferenceById(productId)).thenReturn(product);
        when(shoppingCartRepository.save(cart)).thenReturn(cart);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.get(redisKey)).thenReturn(responseDto);

        shoppingCartService.deleteProductFromShoppingCart(id, productId);

        assertFalse(cart.getProducts().contains(product));
        verify(shoppingCartRepository, times(1)).findById(id);
        verify(shoppingCartRepository, times(1)).getReferenceById(id);
        verify(productRepository, times(1)).getReferenceById(productId);
        verify(redisTemplate, times(2)).opsForValue();
        verify(valueOperations, times(1)).set(any(), any());
        verify(valueOperations, times(1)).get(redisKey);
    }

    @Test
    void givenInValidIdDeleteProductFromShoppingCartThenEx() {

        when(shoppingCartRepository.findById(1L)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> shoppingCartService.deleteProductFromShoppingCart(1L, any()))
                .isInstanceOf(RuntimeException.class);
        verify(shoppingCartRepository, times(1)).findById(1L);
    }

    @Test
    void givenValidIdGetShoppingCartFetchFromRedisThenSuccess() {
        Long id = 1L;
        String redisKey = "shoppingCart-" + id;
        ShoppingCartResponseDto responseDto = new ShoppingCartResponseDto("lorem", new ArrayList<>());

        when(redisTemplate.hasKey(redisKey)).thenReturn(true);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(valueOperations.get(redisKey)).thenReturn(responseDto);

        ShoppingCartResponseDto result = shoppingCartService.getShoppingCart(id);

        assertThat(result).isNotNull();
        assertThat(result.getCartName()).isEqualTo(responseDto.getCartName());

        verify(redisTemplate, times(1)).hasKey(redisKey);
        verify(redisTemplate, times(1)).opsForValue();
        verify(valueOperations, times(1)).get(redisKey);
    }

    @Test
    void givenValidIdGetShoppingCartFetchFromDBThenSuccess() {
        Long shoppingCartId = 1L;
        Long productId = 10L;
        String redisKey = "shoppingCart-" + shoppingCartId;
        AddProductToCartDto dto = new AddProductToCartDto(productId);
        List<Product> products = new ArrayList<>();
        Product product = new Product(
                productId,
                "lorem",
                100,
                "desc",
                Category.builder().name("cat").build(),
                ProductDetails.builder().color("red").imageUrl("uri").build()
        );
        ShoppingCart cart = ShoppingCart.builder().id(shoppingCartId).name("lorem").products(new ArrayList<>()).build();

        ShoppingCartResponseDto responseDto = new ShoppingCartResponseDto("lorem", new ArrayList<>());

        when(redisTemplate.hasKey(redisKey)).thenReturn(false);
        when(shoppingCartRepository.findById(shoppingCartId)).thenReturn(Optional.of(cart));
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);

        ShoppingCartResponseDto shoppingCart = shoppingCartService.getShoppingCart(shoppingCartId);

        assertThat(shoppingCart).isNotNull();
        assertThat(shoppingCart.getCartName()).isEqualTo(responseDto.getCartName());

        verify(redisTemplate, times(1)).opsForValue();
        verify(shoppingCartRepository, times(1)).findById(shoppingCartId);
        verify(valueOperations, times(1)).set(any(), any());

    }

    @Test
    void givenInValidGetShoppingCartFetchFromDBThenEx() {

        when(shoppingCartRepository.findById(1L)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> shoppingCartService.getShoppingCart(1L))
                .isInstanceOf(RuntimeException.class);
        verify(shoppingCartRepository, times(1)).findById(1L);
    }

}