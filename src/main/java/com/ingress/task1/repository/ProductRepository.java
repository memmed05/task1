package com.ingress.task1.repository;

import com.ingress.task1.model.Product;
import com.ingress.task1.projections.ProductProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(nativeQuery = true, value = """
            select c.name as category, count(*) as "count" from product p
                join category c on p.category_id=c.id group by c.name;
                        """)
    List<ProductProjection> getProductsCountByCategory();

    List<Product> findAllByPriceBetween(Integer min, Integer max);

    List<Product> findAllByPriceGreaterThan(Integer min);
    List<Product> findAllByPriceLessThan(Integer max);
}
