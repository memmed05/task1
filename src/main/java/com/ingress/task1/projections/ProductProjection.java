package com.ingress.task1.projections;

import com.ingress.task1.model.Category;

public interface ProductProjection {
    String getCategory();
    Integer getCount();
}
