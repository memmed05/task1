package com.ingress.task1.exception;

import com.ingress.task1.exception.dto.ErrorEnum;

public class ProductNotFoundException extends NotFoundException {

    public ProductNotFoundException() {
        super(ErrorEnum.PRODUCT_NOT_FOUND);
    }
}
