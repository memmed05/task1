package com.ingress.task1.exception;

import com.ingress.task1.exception.dto.ErrorEnum;

public class ShoppingCartNotFoundException extends NotFoundException{

    public ShoppingCartNotFoundException() {
        super(ErrorEnum.SHOPPING_CART_NOT_FOUND);
    }
}
