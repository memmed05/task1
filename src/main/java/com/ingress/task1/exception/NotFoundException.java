package com.ingress.task1.exception;

import com.ingress.task1.exception.dto.ErrorEnum;
import lombok.Getter;

@Getter
public class NotFoundException extends RuntimeException {

    private ErrorEnum code;

    public NotFoundException(ErrorEnum code) {
        this.code = code;
    }
}
