package com.ingress.task1.exception.dto;

import lombok.Getter;

@Getter
public enum ErrorEnum {

    NOT_FOUND_EX("not-found-ex"),
    PRODUCT_NOT_FOUND("product-not-found-ex"),
    SHOPPING_CART_NOT_FOUND("shopping-cart-not-found-ex"),
    ;


    private String value;

    ErrorEnum(String value) {
        this.value = value;
    }
}
