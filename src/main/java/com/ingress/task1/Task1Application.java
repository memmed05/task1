package com.ingress.task1;

import com.ingress.task1.model.Account;
import com.ingress.task1.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;

@SpringBootApplication
@RequiredArgsConstructor
public class Task1Application implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Task1Application.class, args);
	}

	private final AccountRepository accountRepository;

	@Override
	public void run(String... args) throws Exception {
		var a1=new Account(1L,"Mammad", new BigDecimal(500.00), 1);
		var a2=new Account(2L,"Ali", new BigDecimal(500.00), 1);
		accountRepository.save(a1);
		accountRepository.save(a2);

	}
}
