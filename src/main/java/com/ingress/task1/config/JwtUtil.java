package com.ingress.task1.config;

import com.ingress.task1.model.Authority;
import com.ingress.task1.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Component
public class JwtUtil {

    private SecretKey secretKey;

    private static final String TEST_SECRET = "QVVUSF9TRUNSRVQgQVVUSF9TRUNSRVQgQVVUSF9TRUNSRVQgQVVUSF9TRUNSRVQgQVVUSF9TRUNSRVQgQVVUSF9TRUNSRVQgQVVUSF9TRUNSRVQgQVVUSF9TRUNSRVQ="; //AUTH_SECRET x8

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(TEST_SECRET);
        secretKey = Keys.hmacShaKeyFor(keyBytes);

    }

    public Jws<Claims> parseToken(String token) {
        return  Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token);
    }

    public String generateToken(User user) {
        return Jwts.builder()
                .subject(user.getUsername())
                .issuedAt(new Date())
                .expiration(Date.from(Instant.now().plus(5, ChronoUnit.MINUTES)))
                .header()
                .add("type", "access")
                .and()
                .claim("roles", user.getAuthorities().stream().map(Authority::getAuthority).toList())
                .signWith(secretKey)
                .compact();
    }

    public String generateRefreshToken(User user) {
        return Jwts.builder()
                .subject(user.getUsername())
                .issuedAt(new Date())
                .expiration(Date.from(Instant.now().plus(10, ChronoUnit.MINUTES)))
                .header()
                .add("type", "refresh")
                .and()
                .claim("roles", user.getAuthorities().stream().map(Authority::getAuthority).toList())
                .signWith(secretKey)
                .compact();
    }
}
