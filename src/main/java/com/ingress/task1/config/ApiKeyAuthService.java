package com.ingress.task1.config;


import com.ingress.task1.model.User;
import com.ingress.task1.repository.UserRepository;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ApiKeyAuthService implements AuthService {

    private final UserRepository userRepository;


    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest httpServletRequest) {
        String header = httpServletRequest.getHeader("API_KEY");
        if (Objects.nonNull(header)) {
        String key = new String(Base64.getDecoder().decode(header));
        String username = key.substring(key.indexOf(":") + 1);
        User user = userRepository.findUserByUsername(username).orElseThrow(RuntimeException::new);
        return Optional.of(getAuthentication(user));
        }
        return Optional.empty();
    }

    private Authentication getAuthentication(User user) {
        return new UsernamePasswordAuthenticationToken(null, null, user.getAuthorities());
    }
}
