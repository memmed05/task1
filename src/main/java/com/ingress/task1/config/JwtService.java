package com.ingress.task1.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class JwtService implements AuthService{

    private final JwtUtil jwtUtil;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
        String header=request.getHeader("Authorization");
        if (Objects.nonNull(header) && header.startsWith("Bearer ")) {
            String token = header.substring(7);
            try {
                Jws<Claims> claimsJws = jwtUtil.parseToken(token);
                return Optional.of(getAuthentication(claimsJws.getPayload()));
            }catch (Exception e){
                return Optional.empty();
            }
        }
        return Optional.empty();
    }

    private Authentication getAuthentication(Claims claims) {
        List<String> roles = (List<String>) claims.get("roles");
        List<SimpleGrantedAuthority> grantedAuthorities=roles.stream().map(SimpleGrantedAuthority::new).toList();
        return new UsernamePasswordAuthenticationToken(claims, null, grantedAuthorities);
    }
}
