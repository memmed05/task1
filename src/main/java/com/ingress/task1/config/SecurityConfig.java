package com.ingress.task1.config;

import com.ingress.task1.enums.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig {

    private final List<AuthService> authService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.cors(AbstractHttpConfigurer::disable);
        http.csrf(AbstractHttpConfigurer::disable);
        http.sessionManagement(session->session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        http.authorizeHttpRequests(auth-> auth.requestMatchers("/api/v1/auth/*").permitAll());
        http.authorizeHttpRequests(auth-> auth.requestMatchers("/api/v1/test").permitAll());
        http.authorizeHttpRequests(auth-> auth.requestMatchers("/api/v1/auth/key/generate").hasAuthority(Role.ADMIN.name()));
        http.authorizeHttpRequests(auth-> auth.requestMatchers("/api/v1/employee/test-user").hasAnyAuthority(Role.ADMIN.name(), Role.USER.name()));
        http.authorizeHttpRequests(auth-> auth.requestMatchers("/api/v1/employee/test-admin").hasAuthority(Role.ADMIN.name()));
        http.authorizeHttpRequests(auth-> auth.requestMatchers("/api/v1/auth/token/*").hasAnyAuthority(Role.ADMIN.name(), Role.USER.name()));
        http.authorizeHttpRequests(auth-> auth.anyRequest().authenticated());
        http.httpBasic(Customizer.withDefaults());
        http.with(new AuthFilterConfigureAdapter(authService), Customizer.withDefaults());
        return http.build();
    }
}
