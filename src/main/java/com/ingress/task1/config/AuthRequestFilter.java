package com.ingress.task1.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class AuthRequestFilter extends OncePerRequestFilter {

    private final List<AuthService> authService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        for(AuthService service : authService) {
            Optional<Authentication> authentication = service.getAuthentication(request);
            if (authentication.isPresent()) {
                authentication.ifPresent(auth -> SecurityContextHolder.getContext().setAuthentication(auth));
                break;
            }
        }
        filterChain.doFilter(request, response);

    }
}
