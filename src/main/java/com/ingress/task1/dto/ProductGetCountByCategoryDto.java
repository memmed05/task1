package com.ingress.task1.dto;

import com.ingress.task1.model.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductGetCountByCategoryDto {
    private String category;
    private int count;
}
