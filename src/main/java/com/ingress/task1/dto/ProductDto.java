package com.ingress.task1.dto;

import com.ingress.task1.model.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDto implements Serializable {

    private static final Long serialVersionUID = 2L;

    private String name;

    private Integer price;

    private String description;

    private String categoryName;

    private String imageUrl;

    private String color;
}
