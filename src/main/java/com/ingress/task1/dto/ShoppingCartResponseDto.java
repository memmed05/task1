package com.ingress.task1.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ShoppingCartResponseDto implements Serializable {

    private static final Long serialVersionUID = 1L;

    private String cartName;
    private List<ProductDto> products;
}
