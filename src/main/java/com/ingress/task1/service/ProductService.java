package com.ingress.task1.service;

import com.ingress.task1.dto.ProductDto;
import com.ingress.task1.dto.ProductGetCountByCategoryDto;
import com.ingress.task1.model.Category;
import com.ingress.task1.model.Product;
import com.ingress.task1.model.ProductDetails;
import com.ingress.task1.projections.ProductProjection;
import com.ingress.task1.repository.CategoryRepository;
import com.ingress.task1.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    public Long createProduct(ProductDto product) {
        Category category = categoryRepository.findByName(product.getCategoryName())
                .orElse(Category.builder().name(product.getCategoryName()).build());
        Product productEnt = Product.builder()
                .category(category)
                .description(product.getDescription())
                .price(product.getPrice())
                .name(product.getName())
                .productDetails(
                        ProductDetails.builder()
                                .imageUrl(product.getImageUrl())
                                .color(product.getColor())
                                .build()
                )
                .build();
        productEnt = productRepository.save(productEnt);
        return productEnt.getId();
    }

    public List<ProductGetCountByCategoryDto> getProductsCountByCategory() {
        List<ProductProjection> productsCountByCategoryList = productRepository.getProductsCountByCategory();
        return productsCountByCategoryList.stream().map(productProjection ->
                new ProductGetCountByCategoryDto(productProjection.getCategory(),
                        productProjection.getCount())).toList();
    }

    public List<ProductDto> getProductsFilteredByPrice(Integer priceFrom, Integer priceTo) {
        List<Product> products;
        if (priceFrom == null && priceTo == null) {
            products = productRepository.findAll();
        } else if (priceFrom != null && priceTo == null) {
            products = productRepository.findAllByPriceGreaterThan(priceFrom);
        } else if (priceFrom == null && priceTo != null) {
            products = productRepository.findAllByPriceLessThan(priceTo);
        } else {
            products = productRepository.findAllByPriceBetween(priceFrom, priceTo);
        }
        return products.stream()
                .map(product -> new ProductDto(product.getName(),
                        product.getPrice(),
                        product.getDescription(),
                        product.getCategory().getName(),
                        product.getProductDetails().getImageUrl(),
                        product.getProductDetails().getColor()))
                .toList();
    }

    public Page<Product> getAllBySorting(Pageable pageable) {
//        Sort sort = Sort.by(Sort.Direction.fromString(direction), parameter);
//        Pageable pageable = PageRequest.of(page, size, sort);
        return productRepository.findAll(pageable);
    }

    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }
}
