package com.ingress.task1.service;

import com.ingress.task1.config.JwtUtil;
import com.ingress.task1.dto.LoginRequest;
import com.ingress.task1.dto.LoginResponse;
import com.ingress.task1.dto.UserDto;
import com.ingress.task1.enums.Role;
import com.ingress.task1.model.Authority;
import com.ingress.task1.model.User;
import com.ingress.task1.repository.UserRepository;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtil jwtUtil;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findUserByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }

    public void register(UserDto user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.findUserByUsername(user.getUsername()).ifPresentOrElse(
                u->{
                    throw new UsernameNotFoundException(user.getUsername());
                },
                () -> userRepository.save(User.builder()
                        .username(user.getUsername())
                        .password(passwordEncoder.encode(user.getPassword()))
                        .authorities(List.of(new Authority(Role.USER)))
                        .build())
        );

    }

    public LoginResponse login(LoginRequest request) {
        User user=userRepository.findUserByUsername(request.getUsername()).orElseThrow(() -> new UsernameNotFoundException(request.getUsername()));
        passwordEncoder.matches(request.getPassword(), user.getPassword());
        return new LoginResponse(jwtUtil.generateToken(user), jwtUtil.generateRefreshToken(user));
    }

    public LoginResponse renew() {
        Claims claims = (Claims) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = (String) claims.get("sub");
        User user = userRepository.findUserByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
        return new LoginResponse(
                jwtUtil.generateToken(user),
                jwtUtil.generateRefreshToken(user)
        );
    }

    public String generateApiKey(LoginRequest request) {
        User user=userRepository.findUserByUsername(request.getUsername()).orElseThrow(() -> new UsernameNotFoundException(request.getUsername()));
        String saltedKey="myapikey:"+user.getUsername();
        user.setApiKey(Base64.getEncoder().encodeToString(saltedKey.getBytes()));
        userRepository.save(user);
        return user.getApiKey();
    }
}
