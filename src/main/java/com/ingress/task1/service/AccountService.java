package com.ingress.task1.service;

import com.ingress.task1.dto.TransferDto;
import com.ingress.task1.model.Account;
import com.ingress.task1.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class AccountService {


    private final AccountRepository accountRepository;

    @Transactional
    public void transfer(TransferDto request) {
        try {
            Account to = accountRepository.findById(request.getTo()).orElseThrow(() -> new RuntimeException("Account not found"));
            Thread.sleep(5000);
            Account from = accountRepository.findById(request.getFrom()).orElseThrow(() -> new RuntimeException("Account not found"));
            to.setBalance(to.getBalance().subtract(request.getAmount()));
            from.setBalance(from.getBalance().add(request.getAmount()));
            accountRepository.save(to);
            accountRepository.save(from);
            accountRepository.flush();
        } catch (OptimisticLockingFailureException e) {
            log.error("Lock exception", e);
            throw new RuntimeException("Lock exception occurred");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}
