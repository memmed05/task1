package com.ingress.task1.service;

import com.ingress.task1.dto.AddProductToCartDto;
import com.ingress.task1.dto.ProductDto;
import com.ingress.task1.dto.ShoppingCartRequestDto;
import com.ingress.task1.dto.ShoppingCartResponseDto;
import com.ingress.task1.exception.ProductNotFoundException;
import com.ingress.task1.exception.ShoppingCartNotFoundException;
import com.ingress.task1.model.Product;
import com.ingress.task1.model.ShoppingCart;
import com.ingress.task1.repository.ProductRepository;
import com.ingress.task1.repository.ShoppingCartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;
    private final RedisTemplate<String , ShoppingCartResponseDto> redisTemplate;

    public Long createShoppingCart(ShoppingCartRequestDto shoppingCart) {
        ShoppingCart savedEnt = shoppingCartRepository.save(ShoppingCart.builder()
                .name(shoppingCart.getName())
                .build());
        Long id = savedEnt.getId();
        redisTemplate.opsForValue().set(generateShoppingCartId(id), ShoppingCartResponseDto.builder().cartName(shoppingCart.getName()).products(new ArrayList<>()).build());
        return id;
    }

    public List<ProductDto> addProductToShoppingCart(Long id, AddProductToCartDto productDto) {
        ShoppingCart shoppingCart = shoppingCartRepository.findById(id).orElseThrow(ShoppingCartNotFoundException::new);
        shoppingCart.getProducts().add(productRepository.findById(productDto.getProductId())
                .orElseThrow(ProductNotFoundException::new));
        shoppingCartRepository.save(shoppingCart);
        List<Product> products = shoppingCart.getProducts();
        List<ProductDto> productDtoList = products.stream()
                .map(product -> ProductDto.builder()
                        .name(product.getName())
                        .price(product.getPrice())
                        .color(product.getProductDetails().getColor())
                        .description(product.getDescription())
                        .imageUrl(product.getProductDetails().getImageUrl())
                        .categoryName(product.getCategory().getName()).build())
                .toList();
        ShoppingCartResponseDto shoppingCartResponseDto = redisTemplate.opsForValue().get(generateShoppingCartId(id));
        shoppingCartResponseDto.setProducts(productDtoList);
        redisTemplate.opsForValue().set(generateShoppingCartId(id), shoppingCartResponseDto);
        return productDtoList;
    }

    public void deleteProductFromShoppingCart(Long id, Long productId) {
        List<Product> products = shoppingCartRepository.findById(id).orElseThrow(ShoppingCartNotFoundException::new)
                .getProducts();
        products.remove(productRepository.getReferenceById(productId));
        ShoppingCart shoppingCart = shoppingCartRepository.getReferenceById(id);
        shoppingCart.setProducts(products);
        shoppingCartRepository.save(shoppingCart);
        List<ProductDto> productDtoList = products.stream()
                .map(product -> ProductDto.builder()
                        .name(product.getName())
                        .price(product.getPrice())
                        .color(product.getProductDetails().getColor())
                        .description(product.getDescription())
                        .imageUrl(product.getProductDetails().getImageUrl())
                        .categoryName(product.getCategory().getName()).build())
                .toList();
        ShoppingCartResponseDto shoppingCartResponseDto = redisTemplate.opsForValue().get(generateShoppingCartId(id));
        shoppingCartResponseDto.setProducts(productDtoList);
        redisTemplate.opsForValue().set(generateShoppingCartId(id), shoppingCartResponseDto);
    }

    public ShoppingCartResponseDto getShoppingCart(Long id) {
        if (Boolean.TRUE.equals(redisTemplate.hasKey(generateShoppingCartId(id)))) {
            return redisTemplate.opsForValue().get(generateShoppingCartId(id));
        }
        ShoppingCart shoppingCart = shoppingCartRepository.findById(id).orElseThrow(ShoppingCartNotFoundException::new);
        List<Product> products = shoppingCart.getProducts();
        ShoppingCartResponseDto shoppingCartResponseDto = new ShoppingCartResponseDto(shoppingCart.getName(), products.stream()
                .map(product -> ProductDto.builder()
                        .name(product.getName())
                        .price(product.getPrice())
                        .color(product.getProductDetails().getColor())
                        .description(product.getDescription())
                        .imageUrl(product.getProductDetails().getImageUrl())
                        .categoryName(product.getCategory().getName()).build())
                .toList());
        redisTemplate.opsForValue().set(generateShoppingCartId(id), shoppingCartResponseDto);
        return shoppingCartResponseDto;
    }

    private String generateShoppingCartId(Long id) {
        return "shoppingCart-" + id;
    }
}
