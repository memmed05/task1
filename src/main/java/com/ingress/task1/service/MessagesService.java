package com.ingress.task1.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class MessagesService {

    private final MessageSource messageSource;

    public String getMessage(String code, String language, String... args) {
        return messageSource.getMessage(code, args, Locale.of(language));
    }

}
