package com.ingress.task1.controller;

import com.ingress.task1.dto.ProductDto;
import com.ingress.task1.dto.ProductGetCountByCategoryDto;
import com.ingress.task1.model.Product;
import com.ingress.task1.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/product")
public class ProductController {

    private final ProductService productService;

    @PostMapping
    public ResponseEntity<Long> createProduct(@RequestBody ProductDto product) {
        return new ResponseEntity<>(productService.createProduct(product), HttpStatus.CREATED);
    }

    @GetMapping("/category")
    public ResponseEntity<List<ProductGetCountByCategoryDto>> getProductsCountByCategory() {
        return ResponseEntity.ok(productService.getProductsCountByCategory());
    }

    @GetMapping("/price")
    public ResponseEntity<List<ProductDto>> getProductsFilteredByPrice(@RequestParam(required = false) Integer priceFrom,
                                                                       @RequestParam(required = false) Integer priceTo) {
        return ResponseEntity.ok(productService.getProductsFilteredByPrice(priceFrom,priceTo));
    }

    @GetMapping
    public ResponseEntity<Page<Product>> getAllBySorting(Pageable pageable) {
        return ResponseEntity.ok(productService.getAllBySorting(pageable));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
        return ResponseEntity.noContent().build();
    }
}
