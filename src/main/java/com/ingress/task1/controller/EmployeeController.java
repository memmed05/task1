package com.ingress.task1.controller;

import com.ingress.task1.model.Employee;
import com.ingress.task1.repository.EmployeeRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/employee")
public class EmployeeController {

    private final EmployeeRepository employeeRepository;


    @GetMapping("/1")
    public Employee getEmployee() {
        return employeeRepository.findById(1L).orElse(null);
    }

    @GetMapping("/t/1")
    @SneakyThrows
    public Employee getEmployeeThread() {
        Thread.sleep(500);
        return employeeRepository.findById(1L).orElse(null);
    }

    @GetMapping("/db/1")
    @Transactional
    @SneakyThrows
    public Employee getEmployeeDB() {
        Thread.sleep(500);
        return employeeRepository.findById(1L).orElse(null);
    }

    @GetMapping("test-user")
    public String testUser() {
        return "test";
    }

    @GetMapping("test-admin")
    public String testAdmin() {
        return "test";
    }

    @GetMapping("test")
    public String test() {
        return "test";
    }



}
