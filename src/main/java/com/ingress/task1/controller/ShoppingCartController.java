package com.ingress.task1.controller;

import com.ingress.task1.dto.ProductDto;
import com.ingress.task1.dto.ShoppingCartRequestDto;
import com.ingress.task1.dto.AddProductToCartDto;
import com.ingress.task1.dto.ShoppingCartResponseDto;
import com.ingress.task1.service.ShoppingCartService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/shopping-cart")
@RequiredArgsConstructor
public class ShoppingCartController {

    private final ShoppingCartService shoppingCartService;

    @PostMapping
    public ResponseEntity<Long> createShoppingCart(@RequestBody @Valid ShoppingCartRequestDto shoppingCart) {
        return new ResponseEntity<>(shoppingCartService.createShoppingCart(shoppingCart), HttpStatus.CREATED);
    }

    @PostMapping("/{id}/product")
    public ResponseEntity<List<ProductDto>> addProductToShoppingCart(@PathVariable Long id, @RequestBody @Valid AddProductToCartDto product) {
        return ResponseEntity.ok(shoppingCartService.addProductToShoppingCart(id,product));
    }

    @DeleteMapping("/{id}/product/{productId}")
    public ResponseEntity<Void> deleteProductFromShoppingCart(@PathVariable Long id, @PathVariable Long productId) {
        shoppingCartService.deleteProductFromShoppingCart(id,productId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ShoppingCartResponseDto> getShoppingCart(@PathVariable Long id) {
        return ResponseEntity.ok(shoppingCartService.getShoppingCart(id));
    }
}
