package com.ingress.task1.controller;

import com.ingress.task1.dto.TransferDto;
import com.ingress.task1.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping("/api/v1/transfer")
    public void transferV1(@RequestBody TransferDto request) {
        accountService.transfer(request);
    }

    @PostMapping("/api/v2/transfer")
    public void transferV2(@RequestBody TransferDto request) {
        accountService.transfer(request);
    }
}
