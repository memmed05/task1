package com.ingress.task1.controller;

import com.ingress.task1.dto.LoginRequest;
import com.ingress.task1.dto.LoginResponse;
import com.ingress.task1.dto.UserDto;
import com.ingress.task1.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/auth")
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    public void userRegister(@RequestBody UserDto user) {
         userService.register(user);
    }

    @PostMapping("/login")
    public LoginResponse userRegister(@RequestBody LoginRequest request) {
      return userService.login(request);
    }

    @GetMapping("/token/renew")
    public LoginResponse renew() {
        return userService.renew();
    }

    @PostMapping("/key/generate")
    public String generateKey(@RequestBody LoginRequest request) {
        return userService.generateApiKey(request);
    }

}
